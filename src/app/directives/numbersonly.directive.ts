import { Directive,ElementRef,HostListener,Input} from '@angular/core';

@Directive({
    selector:'[numbersonly]'
})
export class NumbersOnlyDirective{
   constructor(private el :ElementRef){
       console.log(el.nativeElement);
   }
   @Input()
   expression:any
 @HostListener('keypress',['$event'])
   onKeyPress(e){
       console.log("Key pressed");
       console.log(e);
       let regex = new RegExp(this.expression);
       if(!regex.test(e.key)){
           e.preventDefault();
       }

   }
}