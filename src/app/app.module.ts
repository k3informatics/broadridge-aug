import { BrowserAnimationsModule,NoopAnimationsModule }
 from '@angular/platform-browser/animations';

import { BrowserModule } from '@angular/platform-browser';

import { FormsModule,ReactiveFormsModule } from '@angular/forms'
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {DataGridModule,
        PanelMenuModule, 
        PanelModule,
        DialogModule,
        TabViewModule} from 'primeng/primeng'
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LogoComponent } from './logo/logo.component';
import { RegisterComponent } from './register/register.component';
import { DataService } from './services/data.service';
import { RouterModule } from '@angular/router';
import { ProductComponent } from './product/product.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { LoginComponent } from './login/login.component';
import { FormatPipe } from './format.pipe';
import { NumbersOnlyDirective } from './directives/numbersonly.directive';
import { AlphabetsDirective } from './alphabets.directive';
import { DatepickerDirective } from './datepicker.directive';

var routes = [{ path: 'register', component: RegisterComponent },
{path:'login',component:LoginComponent},
{ path: 'products', component: ProductComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LogoComponent,
    RegisterComponent,
    ProductComponent,
    DropdownComponent,
    LoginComponent,
    FormatPipe,
    NumbersOnlyDirective,
    AlphabetsDirective,
    DatepickerDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    DataGridModule,PanelMenuModule,
    BrowserAnimationsModule,
     NoopAnimationsModule,
        PanelModule,
        DialogModule,
        TabViewModule,
    RouterModule.forRoot(routes)
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }






