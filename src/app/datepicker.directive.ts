import { Directive,ElementRef } from '@angular/core';
declare var jQuery:any;
@Directive({
  selector: '[datepicker]'
})
export class DatepickerDirective {

  constructor(private el:ElementRef) { 
    jQuery(this.el.nativeElement).datepicker();
  }




}
