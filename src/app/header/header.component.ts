import { DataService } from '../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
   cart:any;
  constructor(private dataSvc:DataService) { 
    //this.cart=this.dataSvc.getCartItems();
    this.cart=[];
    this.dataSvc.getCartDataAsObservable()
    .subscribe(x=>{
      this.cart.push(x);
    })
  }

  ngOnInit() {
  }

}
