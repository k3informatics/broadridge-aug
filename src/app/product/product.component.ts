import { DataService } from '../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products: any;
selectedProduct:any;
displayDialog: boolean;
  constructor(private productSvc:DataService) {
    this.productSvc.getProductsFromApi()
    .then(res=>{
      this.products = res.products.map(x=>{
        x.quantity=0;
        return x;
      })
    })
    .catch(res=>{
    });
  }

  ngOnInit() {
  }
  add(item){
    item.quantity++;
    //this.productSvc.handleCart(item);
    this.productSvc.handleCartObservable(item);
  }
  remove(item){
    if(item.quantity>0){
      item.quantity--;
    }
    this.productSvc.handleCartObservable(item);
    //this.productSvc.handleCart(item);
  }
selectProduct(item) {
        this.selectedProduct = item;
        this.displayDialog = true;
    }
    
    onDialogHide() {
        this.selectedProduct = null;
    }



}
