import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   loginForm:FormGroup
  constructor(private fb:FormBuilder) {
    this.loginForm= fb.group({
      username:[null, Validators.compose(
        [Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10)]
      )]
    });

   }

  ngOnInit() {
  }

  login(){
   
    console.log(this.loginForm.controls['username'].value);
  }

}
