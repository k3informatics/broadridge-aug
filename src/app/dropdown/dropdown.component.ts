import { Component, OnInit,Input ,Output,EventEmitter} from '@angular/core';
import { DropdownConfig } from "../common/dropdown.config";


@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {
  @Input()
  configuration:DropdownConfig;

  @Output()
  dropdownchange:EventEmitter<String>

  selectedvalue:String
  valuechanged(){
    this.dropdownchange.emit(this.selectedvalue);  
  }

  constructor() {
    this.configuration= new DropdownConfig();
    this.dropdownchange = new EventEmitter<String>();
   }

  ngOnInit() {
  }

}
