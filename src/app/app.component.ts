import { Observable } from 'rxjs/Rx';
import { Component } from '@angular/core';
import 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  Rx:any;
  price="30000";
  phone="1234567890";
  numbersOnlyRegex=/[0-9]$/;
 dateOfBirth = new Date();

loadTime(){
  setInterval(()=>{
 this.dateOfBirth= new Date();
  },1000)
}
  constructor(){
    this.loadTime();
    // this.Rx= new Observable(); 
    // this.example1();
  }
  //example1(){
//   //emit four strings
// let example = this.Rx.Observable.of('WAIT','ONE','SECOND','Last will display');
// /*
//     Only emit values after a second has passed between the last emission, 
//     throw away all other values
// */
// const debouncedExample = example.debounce(() => this.Rx.Observable.timer(1000));
// /*
//     In this example, all values but the last will be omitted
//     output: 'Last will display'
// */
// const subscribe = debouncedExample.subscribe(val => console.log(val));

// //emit value every 1 second, ex. 0...1...2
// const interval = this.Rx.Observable.interval(1000);
// //raise the debounce time by 200ms each second
// const debouncedInterval = interval.debounce(val => this.Rx.Observable.timer(val * 200))
// /*
//   After 5 seconds, debounce time will be greater than interval time,
//   all future values will be thrown away
//   output: 0...1...2...3...4......(debounce time over 1s, no values emitted)
// */
// const subscribeTwo = debouncedInterval.subscribe(val => console.log(`Example Two: ${val}`));
//   }
}
