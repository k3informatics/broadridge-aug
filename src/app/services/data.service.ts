
import { AppConstants } from '../constants/app.constants';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable,Subject } from 'rxjs/Rx';
//import {Observable} from 'rxjs'
import 'rxjs';
//Decorate the class as Injectable.
@Injectable()
export class DataService {
    private cartItems = [];
    subject:Subject<any>;
    constructor(private httpSvc: Http) {
        this.subject= new Subject<any>();
    }
    getCartItems() {
        return this.cartItems;
    }
    handleCartObservable(data){
        this.subject.next(data);
    }
     getCartDataAsObservable(){
        return this.subject.asObservable();
     };

    handleCart(data) {
        var isItemExistInCart = false;
        var index=0;
        var indexToRemove=-1;
        this.cartItems.forEach(x => {
            index++;
            if (x._id == data._id) {
                x = data;
                isItemExistInCart = true;
            }
            if(x.quantity==0){
                indexToRemove=index;
            }
        });
        if (!isItemExistInCart) {
            this.cartItems.push(data);
        }
        else{
        this.cartItems.splice(indexToRemove,1);
        }
    }
    getCountriesFromApi(url) {
        let uri = AppConstants.BASE_URL + url;
        return this.httpSvc.get(uri)
            .map(data => {
                return data.json();
            },
            err => {
                return err.json();
            })
            .toPromise()
    }

    getCountriesFromApiAsObservable(url):Observable<any> {
        let uri = AppConstants.BASE_URL + url;
        return this.httpSvc.get(uri)
            .map(data => {
                return data.json();
            },
            err => {
                return err.json();
            });
            //.toPromise()
    }

    getCountries(): Array<any> {
        return [{ name: "India", code: "IN" },
        { name: "United States", code: "US" },
        { name: "Canada", code: "CA" }];
    }
    getStatesByCounty(countryCode) {

    }

    getProductsFromApi() {
        let uri = AppConstants.BASE_URL + AppConstants.PRODUCTS_API;
        return this.httpSvc.get(uri)
            .map(data => {
                return data.json();
            },
            err => {
                return err.json();
            })
            .toPromise()
    }


}