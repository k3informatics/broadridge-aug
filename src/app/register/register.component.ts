import { DropdownConfig } from '../common/dropdown.config';
import { AppConstants } from '../constants/app.constants';

import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  profile: any
  countries: Array<any>
  dropdownConfig:DropdownConfig
  //dataSerivce:DataService;
  constructor(private dataSvc:DataService) {
    this.profile = {};
    //this.dataSerivce= dataSvc;
    //this.countries= this.dataSvc.getCountries();
    this.getCountriesAsync();
    this.dropdownConfig=new DropdownConfig();
    this.dropdownConfig.cssClass="form-control";
   
  }

  countryChanged(val){
    console.log(val);
  }

  
  register() {
    console.log(this.profile);
  }

  ngOnInit() {
  }

  getCountriesAsync(){
    this.dataSvc.getCountriesFromApiAsObservable(AppConstants.COUNTRIES_API)
    .subscribe(
      (res)=>{
       console.log(res);
      this.countries=res;
      this.dropdownConfig.data= res.map(x=>{
        let y={
        value:x.code,
        text :x.name
        }
        return y;
      });
      },
    (err)=>{
      console.log(err);
    }
      
    );
    // this.dataSvc.getCountriesFromApi(AppConstants.COUNTRIES_API)
    // .then(res=>{
    //   console.log(res);
    //   this.countries=res;
    //   this.dropdownConfig.data= res.map(x=>{
    //     let y={
    //     value:x.code,
    //     text :x.name
    //     }
    //     return y;
    //   })
    // })
    // .catch(res=>{
    //   console.log(res);
    // })
  };

}
