import { BroadkartPage } from './app.po';

describe('broadkart App', () => {
  let page: BroadkartPage;

  beforeEach(() => {
    page = new BroadkartPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
